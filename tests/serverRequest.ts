import { ServerRequest } from 'https://deno.land/x/oak/deps.ts'

const baseUrl = '/'

const createServerRequest = (path: string, method: string) => {
  const serverRequest = new ServerRequest()
  serverRequest.url = baseUrl + path
  serverRequest.method = method

  return serverRequest
}

export default createServerRequest
