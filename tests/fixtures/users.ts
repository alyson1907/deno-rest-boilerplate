const fixtures = {
  user: {
    name: 'Alyson',
    age: 24
  },
  throwingFunc: () => {
    throw new Error('Error coming from throwingFunc fixture')
  }
}

export default fixtures
