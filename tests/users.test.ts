import { assertEquals, assertThrows } from 'https://deno.land/std/testing/asserts.ts'
import { Request, Response } from 'https://deno.land/x/oak/mod.ts'
import fixtures from './fixtures/users.ts'
import UsersService from '../src/services/users.ts'
import serverRequest from './serverRequest.ts'

function setup() {
  // ...amazing code to be run before test suite
}

Deno.test('Some fake test', async () => {
  assertEquals(fixtures.user.name, 'Alyson')
})

Deno.test('Another fake test', async () => {
  assertThrows(fixtures.throwingFunc)
})

Deno.test('GET users', async () => {
  const req = new Request(serverRequest('/users', 'get'))
  const res = new Response(req)

  await UsersService.getUser({ request: req, response: res })
  // Response body
  const body = res.body
  console.log(body)
  // Do some assertion based on `body`
})

