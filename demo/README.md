This folder is only used to store demonstration **images** and **gifs** for the repository

The `demo/` directory can be **safely** deleted. 
