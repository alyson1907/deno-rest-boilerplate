import { Router } from 'https://deno.land/x/oak/mod.ts'
import UsersService from './services/users.ts'

const router = new Router()

// Setting routes
router
  .get('/users', UsersService.getUser)
  .post('/', UsersService.createUser)

export default router
