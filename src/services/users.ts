
import { Request, Response } from 'https://deno.land/x/oak/mod.ts'
const mockAPI = 'https://5ed43c80fffad10016056ba6.mockapi.io'


const getUser = async ({ request, response }: { request: Request, response: Response }) => {
  // ...doing something amazing
  const data = await fetch(`${mockAPI}/users`)
  response.status = 200
  response.body = await data.json()
}

const createUser = async ({ params, request, response }: { params:any, request: Request, response: Response }) => {
  const data = await request.body()
  // ...amazingly dealing with data
  response.status = 201
  response.body = {
    message: 'Ok!'
  }
}

export default {
  getUser,
  createUser
}
