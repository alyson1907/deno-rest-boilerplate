![](./demo/deno.jpg)
## Description
This repository is a simple boilerplate for REST APIs using [Deno](https://deno.land/)

## Deno Installation
You can find the installation guide for Deno [here](https://deno.land/#installation)

## Running
To start the server:
```sh
deno run --allow-read=./ --allow-net --allow-env src/server.ts
```
![](./demo/start.gif)

## Testing
To run automated tests:
```sh
deno test --alow-net
```
![](./demo/test.gif)

## Flags and Permissions
When using Deno, you need to explicitly provide permissions to the packages and scripts you will run. You can see the permission flags used in this boilerplate below:
- `--allow-net`: Since this boilerplate implements a REST API, we need to allow it to have access to the network.
- `--allow-env`: allows the service to have access to environment variables provided by `Deno.env`
- `--allow-read`
